defmodule RsvpWeb.AutorisedPlug do
  import Plug.Conn
  import Phoenix.Controller

  def init(opts) do
    opts
  end
  
  def call(conn, name) do
    user_name = conn.cookies["user_name"]

    authorise_user(conn, user_name, name)
  end
  
  defp authorise_user(conn, nil, _) do
    conn
    |> redirect(to: "/login")
    |> halt
  end

  defp authorise_user(conn, user_name, name) when user_name === name do
    conn
  end
  
  defp authorise_user(conn, _, _), do: authorise_user(conn, nil, nil)
end