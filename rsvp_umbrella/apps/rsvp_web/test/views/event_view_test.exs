defmodule RsvpWeb.EventViewTest do
    use RsvpWeb.ConnCase, async: true

    @tag current: true
    test "Should convert a date to a D/M/YY format" do
      date = Ecto.DateTime.from_erl({{2017, 12, 03}, {00, 00, 00}})
      formatted = RsvpWeb.EventView.format_date(date)
      assert formatted == "03/12/2017"
    end

    @tag current: true
    test "Should convert a time to a H:M format" do
      date = Ecto.DateTime.from_erl({{2017, 12, 03}, {09, 00, 00}})
      formatted = RsvpWeb.EventView.format_time(date)
      assert formatted == "09:00:00"
    end
end