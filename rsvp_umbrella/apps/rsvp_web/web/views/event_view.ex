defmodule RsvpWeb.EventView do
    use RsvpWeb.Web, :view

    def format_date(date) do
        {{y, m, d}, _} = Ecto.DateTime.to_erl(date)
        "#{pad_leading(d)}/#{pad_leading(m)}/#{y}"
    end

    def format_time(date) do
      {_, {h, m, s}} = Ecto.DateTime.to_erl(date)
      "#{pad_leading(h)}:#{pad_leading(m)}:#{pad_leading(s)}"
    end

    defp pad_leading(num) do
        num
        |> Integer.to_string
        |> String.pad_leading(2, "0")
    end
end